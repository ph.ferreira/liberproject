import { Component, OnInit } from '@angular/core';

class Livros {
  id_livro: number;
  titulo: string;
  autor: string;
  anoDePublicacao: number;
  preco: number;
  capa: string;
  estado: string;
  descricao: string;
  url: string;
  vendedor: string;
  comentario: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {

  livros: Livros[];

  constructor() { }

  ngOnInit() {
    this.livros = [
      {
        id_livro: 1,
        titulo: 'Senhor dos Anéis - O Retorno do Rei',
        autor: 'J.R.R.Tolkien',
        anoDePublicacao: 1955,
        preco: 60.00,
        capa: '../../assets/Liber Imagens/Tolkien/Retorno do reijpg.jpg',
        estado: 'Novo',
        descricao: 'O Retorno do Rei é a terceira parte da grande obra de ficção fantástica de J. R. R. Tolkien, O Senhor dos Anéis. É impossível transmitir ao novo leitor todas as qualidades e o alcance do livro. Alterdamente cômica, singela, épica, monstruosa e diabólica, a narrativa desenvolve-se em meio a inúmeras mudanças de cenários e de personagens, num mundo imaginário absolutamente em seus detalhes. Tolkien criou em O Senhor dos Anéis uma nova mitologia, num mundo inventado que demonstrou possuir um poder de atração atemporal.',
        url: '/livro',
        vendedor: 'Livraria Livros',
        comentario: 'Esse livro é perfeito! O Tolkien tem uma narrativa tão incrível que é impossível não se prender na obra. Livro excelente! Recomendo a todos meus amigos.'
      },
      {
        id_livro: 2,
        titulo: 'Harry Potter e a Pedra Filosofal',
        autor: 'J.k.Rolling',
        anoDePublicacao: 1997,
        preco: 40.00,
        capa: '../../assets/Liber Imagens/Rolling/Pedr filosofal.jpg',
        estado: 'Usado',
        descricao: 'Livro bem legal',
        url: '',
        vendedor: 'Livraria Livros',
        comentario: ''
      },
      {
        id_livro: 3,
        titulo: 'Game of Thrones - A Guerra dos Tronos',
        autor: 'G.R.R.Martin',
        anoDePublicacao: 1996,
        preco: 70.00,
        capa: '../../assets/Liber Imagens/Martin/gm1.jpg',
        estado: 'Novo',
        descricao: 'Livro muito bom',
        url: '',
        vendedor: 'Livraria Livros',
        comentario: ''
      },
      {
        id_livro: 3,
        titulo: 'Game of Thrones - A Guerra dos Tronos',
        autor: 'G.R.R.Martin',
        anoDePublicacao: 1996,
        preco: 70.00,
        capa: '../../assets/Liber Imagens/Martin/gm1.jpg',
        estado: 'Novo',
        descricao: 'Livro muito bom',
        url: '',
        vendedor: 'Livraria Livros',
        comentario: ''
      },
      {
        id_livro: 3,
        titulo: 'Game of Thrones - A Guerra dos Tronos',
        autor: 'G.R.R.Martin',
        anoDePublicacao: 1996,
        preco: 70.00,
        capa: '../../assets/Liber Imagens/Martin/gm1.jpg',
        estado: 'Novo',
        descricao: 'Livro muito bom',
        url: '',
        vendedor: 'Livraria Livros',
        comentario: ''
      }
    ]
  }

}

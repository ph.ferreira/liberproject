<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Criando a migration create_books com os atributos seguindo a modelagem BD*/
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('titulo');
            $table->string('autor');
            $table->integer('anoDePublicacao')->nullable();
            $table->float('preco');
            $table->string('categoria');
            $table->string('descricao')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();

            $table->timestamps();
        });

        /* Comando para excluir o livro caso o usuário seja excluído */
        Schema::table('books', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');            
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}

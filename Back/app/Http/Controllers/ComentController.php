<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Importação da model Coment
use App\Models\Coment;

class ComentController extends Controller
{
    public function create(Request $request){
        $coment = new Coment;
        
        $coment->createComent($request);
        
        return response()->json(['coment'=>$coment],200);
    }

    public function index(){
        $coment = Coment::all();

        return response()->json(['coment'=>$coment],200);
    }

    public function show($id){
        $coment = Coment::find($id);

        return response()->json(['coment'=>$coment],200);
    }

    public function update(Request $request, $id){
        $coment = Coment::find($id);
        
        $coment->updatecoment($request);

        return response()->json(['coment'=>$coment],200);
    }

    public function destroy($id){
        Coment::destroy($id);

        return response()->json(['Comentario deletado com sucesso'],200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Importação da model Buy
use App\Models\Buy;

class BuyController extends Controller
{
    public function create(Request $request){
        $buy = new Buy;
        
        $buy->createBuy($request);
        
        return response()->json(['buy'=>$buy],200);
    }

    public function index(){
        $buy = Buy::all();

        return response()->json(['book'=>$buy],200);
    }

    public function show($id){
        $buy = Buy::find($id);

        return response()->json(['buy'=>$buy],200);
    }

    public function update(Request $request, $id){
        $buy = Buy::find($id);

        $buy->updateBuy($request);

        return response()->json(['buy'=>$buy],200);
    }

    public function destroy($id){
        Buy::destroy($id);

        return response()->json(['Compra deletada com sucesso'],200);
    }
}

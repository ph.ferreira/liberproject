<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Importação da model Book
use App\Models\Book;

class BookController extends Controller
{
    public function create(Request $request){
        $book = new Book;

        $book->createBook($request);
        
        return response()->json(['book'=>$book],200);
    }

    public function index(){
        $books = Book::all();

        return response()->json(['book'=>$books],200);
    }

    public function show($id){
        $book = Book::find($id);

        return response()->json(['book'=>$book],200);
    }

    public function update(Request $request, $id){
        $book = Book::find($id);
        
        $book->updateBook($request);

        return response()->json(['book'=>$book],200);
    }

    public function destroy($id){
        Book::destroy($id);

        return response()->json(['Livro deletado com sucesso'],200);
    }
}

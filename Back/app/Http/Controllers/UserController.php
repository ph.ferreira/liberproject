<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Importação da model User
use App\Models\User;

class UserController extends Controller
{
    public function create(Request $request){
        $user = new User;        
        $user->createUser($request);
        
        return response()->json(['user'=>$user],200);
    }

    public function index(){
        $users = User::all();

        return response()->json(['user'=>$users],200);
    }

    public function show($id){
        $user = User::find($id);

        return response()->json(['user' =>$user ],200);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        
        $user->updateUser($request);

        return response()->json(['user'=>$user],200);
    }

    public function destroy($id){
        User::destroy($id);

        return response()->json(['Usuário deletado com sucesso'],200);
    }
}

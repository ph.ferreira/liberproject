<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function createBook($request){
        $this->titulo = $request->titulo;
        $this->autor = $request->autor;
        $this->anoDePublicacao = $request->anoDePublicacao;
        $this->preco = $request->preco;
        $this->categoria = $request->categoria;
        $this->descricao = $request->descricao;
        $this->save();
    }

    public function updateBook($request){
        if($request->titulo){
            $this->titulo = $request->titulo;
        }

        if($request->autor){
            $this->autor = $request->autor;
        }

        if($request->anoDePublicacao){
            $this->anoDePublicacao = $request->anoDePublicacao;
        }

        if($request->preco){
            $this->preco = $request->preco;
        }

        if($request->categoria){
            $this->categoria = $request->categoria;
        }

        if($request->descricao){
            $this->descricao = $request->descricao;
        }

        $this->save();
    }

    // Criando a relação Possuir Livro (implementada em Buy.php)
    public function pertenceCompra(){
        return $this->belongsTo('App\Models\Buy');
    }

    // Criando a relação Postar Comentário (implementada em Coment.php)
    public function comentarioPostado(){
        return $this->hasMany('App\Models\Coment');
    }
}


<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    public function createUser($request){
        $this->nome = $request->nome;
        $this->cpf = $request->cpf;
        $this->email = $request->email;
        $this->password = $request->password;
        $this->cep = $request->cep;
        $this->complemento_endereco = $request->complemento_endereco;
        $this->save();
    }

    public function updateUser($request){
        if($request->nome){
            $this->nome = $request->nome;
        }

        if($request->cpf){
            $this->cpf = $request->cpf;
        }

        if($request->email){
            $this->email = $request->email;
        }

        if($request->password){
            $this->password = $request->password;
        }

        if($request->cep){
            $this->cep = $request->cep;
        }

        if($request->complemento_endereco){
            $this->complemento_endereco = $request->complemento_endereco;
        }

        $this->save();
    }

    // Criando a relação Cadastro da modelagem BD
    public function cadastrarLivro(){
        return $this->hasMany('App\Models\Book');
    }

    // Criando a relação Realizar Compra da modelagem BD
    public function realizarCompra(){
        return $this->hasMany('App\Models\Buy');
    }

    // Criando a relação Realizar Comentário da modelagem BD
    public function realizarComentario(){
        return $this->hasMany('App\Models\Coment');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

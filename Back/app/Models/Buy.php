<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buy extends Model
{
    public function createBuy($request){
        $this->valor_total = $request->valor_total;
        $this->user_id = $request->user_id;
        $this->book_id = $request->book_id;
        $this->data = $request->data;
        $this->save();
    }

    public function updateBuy($request){
        if($request->valor_total){
            $this->valor_total = $request->valor_total;
        }

        if($request->data){
            $this->data = $request->data;
        }
        $this->save();
    }

    // Criando a relação Possuir Livro na modelagem BD
    public function possuirLivro(){
        return $this->hasMany('App\Models\Book');
    }

    //Criando a relação Realizar Compra (implementada em User.php) no sentido inverso
    public function compraRealizada(){
        return $this->belongsTo('App\Models\User');
    }
}
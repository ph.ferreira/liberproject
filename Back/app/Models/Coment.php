<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coment extends Model
{
    public function createComent($request){
        $this->user_id = $request->user_id;
        $this->book_id = $request->book_id;
        $this->comentario = $request->comentario;
        $this->nota = $request->nota;
        $this->save();
    }

    public function updateComent($request){
        if($request->user_id){
            $this->user_id = $request->user_id;
        }

        if($request->book_id){
            $this->book_id = $request->book_id;
        }

        if($request->comentario){
            $this->comentario = $request->comentario;
        }

        if($request->nota){
            $this->nota = $request->nota;
        }

        $this->save();
    }

    // Criando a relação Postar Comentário da modelagem BD
    public function postarComentario(){
        return $this->belongsTo('App\Models\Book');
    }

    // Criando a relação Realizar Comentário (implementada em User.php) no sentido inverso
    public function comentarioFeito(){
        return $this->belongsTo('App\Models\User');
    }
}

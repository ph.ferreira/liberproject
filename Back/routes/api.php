<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BookController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ComentController;
use App\Http\Controllers\BuyController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Rotas do BookController
Route::get('books', [BookController::class, 'index']);
Route::get('books/{id}', [BookController::class, 'show']);
Route::post('books', [BookController::class, 'create']);
Route::put('books/{id}', [BookController::class, 'update']);
Route::delete('books/{id}', [BookController::class, 'destroy']);

// Rotas do UserController
Route::get('users', [UserController::class, 'index']);
Route::get('users/{id}', [UserController::class, 'show']);
Route::post('users', [UserController::class, 'create']);
Route::put('users/{id}', [UserController::class, 'update']);
Route::delete('users/{id}', [UserController::class, 'destroy']);

// Rotas do ComentController
Route::get('coments', [ComentController::class, 'index']);
Route::get('coments/{id}', [ComentController::class, 'show']);
Route::post('coments', [ComentController::class, 'create']);
Route::put('coments/{id}', [ComentController::class, 'update']);
Route::delete('coments/{id}', [ComentController::class, 'destroy']);

// Rotas do BuyController
Route::get('buys', [BuyController::class, 'index']);
Route::get('buys/{id}', [BuyController::class, 'show']);
Route::post('buys', [BuyController::class, 'create']);
Route::put('buys/{id}', [BuyController::class, 'update']);
Route::delete('buys/{id}', [BuyController::class, 'destroy']);